import React from "react"
import { render } from "react-dom"
import "./index.css"
import Todolist from "./Container/Todolist/Todolist"
import { Provider } from "react-redux"
import { createStore } from "redux"
import { todoListReducer } from "./Redux/todolistredux/reducer"

const store = createStore(todoListReducer)

render(
  <Provider store={store}>
    <Todolist />
  </Provider>,
  document.getElementById("root")
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
