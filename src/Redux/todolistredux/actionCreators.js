import { actionType } from "./actionType"

export const saveTodoList = (newTodolist) => {
  return {
    type: actionType.SAVE_TODO_LIST,
    newTodolist,
  }
}

export const saveTodoListIsComplete = (newTodolistIsComplete) => {
  return {
    type: actionType.SAVE_TODO_LIST_COMPLETE,
    newTodolistIsComplete,
  }
}

export const saveTodoListUnComplete = (newTodolistUnComplete) => {
  return {
    type: actionType.SAVE_TODO_LIST_UN_COMPLETE,
    newTodolistUnComplete,
  }
}
