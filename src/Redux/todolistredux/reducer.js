import { actionType } from "./actionType"

export const initialState = {
  todoList: [],
  todoListIsComplete: [],
  todoListUnComplete: [],
}

export const todoListReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.SAVE_TODO_LIST:
      return {
        ...state,
        todoList: action.newTodolist,
      }
    case actionType.SAVE_TODO_LIST_COMPLETE:
      return {
        ...state,
        todoListIsComplete: action.newTodolistIsComplete,
      }
    case actionType.SAVE_TODO_LIST_UN_COMPLETE:
      return {
        ...state,
        todoListUnComplete: action.newTodolistUnComplete,
      }

    default:
      return state
  }
}
