import styled from "styled-components"
export const Container = styled.div`
  display: flex;
  justify-content: center;
  text-align: center;
  border-radius: 8px;
  border: 1px solid rgba(167, 148, 192, 0.3);
  box-shadow: 0 2px 10px 0 rgba(0, 0, 0, 0.1);
  transition: all 0.2s ease-in-out;
  transform: scale(1);
  /* width: 800px; */
  min-width: 400px;
  height: auto;
  margin: 20px;
  box-sizing: border-box;
  background-color: #fff;
  padding: 40px;

  .button-style {
    background-color: #4caf50; /* Green */
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;

    &:hover {
      box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
        0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }
  }

  .text-color-red {
    color: red;
  }
`
export const TodolistContainerStyle = styled.div`
  /* width: 100%; */
  display: grid;
  justify-content: center;
  grid-row-gap: 25px;

  .style-Checkbox {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
`
export const Button = styled.button`
  background-color: #4caf50;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  border-radius: 12px;
`
