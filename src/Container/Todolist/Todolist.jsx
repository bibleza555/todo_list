import React, { useState, useEffect } from "react"
import { useSelector, useDispatch } from "react-redux"
import {
  saveTodoList,
  saveTodoListIsComplete,
  saveTodoListUnComplete,
} from "../../Redux/todolistredux/actionCreators"
import { TodolistContainerStyle, Container } from "./TodolistStyle"
import Checkbox from "@material-ui/core/Checkbox"
import TextField from "@material-ui/core/TextField"
import Select from "@material-ui/core/Select"
import MenuItem from "@material-ui/core/MenuItem"
import IconButton from "@material-ui/core/IconButton"
import DeleteIcon from "@material-ui/icons/Delete"

const Todolist = (props) => {
  const dispatch = useDispatch()

  const allTodoList = useSelector((state) => state.todoList)
  
  const [error, setError] = useState(false)
  const [detail, setDetail] = useState("")
  const [selected, setSelected] = useState("all")
  const [todoList, setTodoList] = useState(allTodoList)

  useEffect(() => {
    dispatch(saveTodoList(todoList))
    // eslint-disable-next-line
  }, [todoList])

  useEffect(() => {
    const todoListIsCompleteds = todoList.filter((item) => item.isCompleted)
    dispatch(saveTodoListIsComplete(todoListIsCompleteds))
    // eslint-disable-next-line
  }, [todoList])

  useEffect(() => {
    const todoListUnCompleteds = todoList.filter((item) => !item.isCompleted)
    dispatch(saveTodoListUnComplete(todoListUnCompleteds))
    // eslint-disable-next-line
  }, [todoList])

  const addItems = () => {
    if (detail === "") {
      setError(true)
    } else {
      setDetail("")
      setError(false)
      setTodoList([
        ...todoList,
        { id: todoList.length + 1, name: detail, isCompleted: false },
      ])
    }
  }

  const deleteItems = (id) => {
    setTodoList(todoList.filter((item) => item.id !== id))
  }

  const handleChange = (index, item) => {
    const newTodos = [...todoList]
    newTodos[index].isCompleted = !item.isCompleted
    setTodoList(newTodos)
  }

  const todoListIsCompleted = todoList.filter((item) => item.isCompleted)
  const todoListUnCompleted = todoList.filter((item) => !item.isCompleted)

  return (
    <Container>
      <TodolistContainerStyle>
        <h1>Todo List</h1>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={selected}
          onChange={(event) => setSelected(event.target.value)}
        >
          <MenuItem value="all">All</MenuItem>
          <MenuItem value="complete">Complete</MenuItem>
          <MenuItem value="uncomplete">Uncomplete</MenuItem>
        </Select>

        {selected === "all" &&
          todoList.map((item, index) => {
            return (
              <div className="style-Checkbox" key={index}>
                <Checkbox
                  checked={item.isCompleted}
                  name="check"
                  onClick={() => handleChange(index, item)}
                />
                {item.name}
                <IconButton
                  aria-label="Delete"
                  onClick={() => deleteItems(item.id)}
                >
                  <DeleteIcon color="secondary" />
                </IconButton>
              </div>
            )
          })}

        {selected === "complete" &&
          todoListIsCompleted.map((item, index) => {
            return (
              <div className="style-Checkbox" key={index}>
                <Checkbox
                  checked={item.isCompleted}
                  name="check"
                  onClick={() => handleChange(index, item)}
                />
                {item.name}
                <IconButton
                  aria-label="Delete"
                  onClick={() => deleteItems(item.id)}
                >
                  <DeleteIcon />
                </IconButton>
              </div>
            )
          })}

        {selected === "uncomplete" &&
          todoListUnCompleted.map((item, index) => {
            return (
              <div className="style-Checkbox" key={index}>
                <Checkbox
                  checked={item.isCompleted}
                  name="check"
                  onClick={() => handleChange(index, item)}
                />
                {item.name}
                <IconButton
                  aria-label="Delete"
                  onClick={() => deleteItems(item.id)}
                >
                  <DeleteIcon />
                </IconButton>
              </div>
            )
          })}

        <TextField
          id="standard-basic"
          label="Detail"
          value={detail}
          onChange={(event) => setDetail(event.target.value)}
        />
        <button className={"button-style"} onClick={() => addItems()}>
          Add
        </button>
        {error === true && <p className={"text-color-red"}>Required detail</p>}
      </TodolistContainerStyle>
    </Container>
  )
}

export default Todolist
